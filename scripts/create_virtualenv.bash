#!/bin/bash

# -*- coding: utf-8 -*
# Sample Project - This is a demo project designed to display coding ability.
#                  No permission is given to copy, distribute or reuse the code.
#
# Copyright (C) 2015 Žiga Mlinar<zigamlinar@gmail.com>
#

## How to use this scipt:
#
# 1. Run this script from project folder:
#     bash scripts/create_virtualenv.bas

# cd one level up if in scripts dir
CURDIR=$(basename $(pwd))
if [[ ${CURDIR} == 'scripts' ]] ; then
  cd ..
fi

PIP='pip install --upgrade'
PIPV='pip install'

if [[ "${1}" == "--no-cache-dir" ]] ; then
    PIP+=' --no-cache-dir'
    PIPV+=' --no-cache-dir'
fi


REQ_CHOICE=""

if [[ "${1}x" != "x" ]] ; then
  REQ_CHOICE="-${1}"
fi

REQ_FILE="config/requirements${REQ_CHOICE}.conf"

if [[ ! -e ${REQ_FILE} ]] ; then
  echo "Requirements file missing [${REQ_FILE}]"
  exit -1
fi

if [[ "$(which virtualenv)x" == "x" ]] ; then
  echo "virtualenv package missing"
  exit -1
fi
if [[ "$(which pip)x" == "x" ]] ; then
  echo "pip package missing"
  exit -1
fi
if [[ "$(which unzip)x" == "x" ]] ; then
  echo "unzip package missing"
  exit -1
fi

# create new virtualenv
if [[ ! -f ./env/bin/activate ]] ; then
    virtualenv env -p /usr/bin/python2.7
fi

# activate virtual env
source ./env/bin/activate
PYTHONPATH=$(pwd)/env/lib/python2.7/site-packages

# upgrade pip first
${PIP} pip

# install needed packages
${PIP} -r ${REQ_FILE}

# deactivate virtual env
deactivate

# prepare closure compiler
cd env/bin && \
if [[ ! -f compiler-latest.zip ]] ; then
    wget  http://dl.google.com/closure-compiler/compiler-latest.zip && unzip -o compiler-latest.zip
fi
