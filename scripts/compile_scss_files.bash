#!/bin/bash

# -*- coding: utf-8 -*
# Sample Project - This is a demo project designed to display coding ability.
#                  No permission is given to copy, distribute or reuse the code.
#
# Copyright (C) 2015 Žiga Mlinar<zigamlinar@gmail.com>
#

## for automatic compile on change use:
#
# sass --watch static/application/css/application.scss:static/application/css/application.css

SASS="sass"

if [[ "$1" == "-d" ]] ; then
  SASS+=" --watch "
fi

if [[ "$(which sass)x" == "x" ]] ; then
  echo "sass package missing"
  exit -1
fi


echo "Compiling scss files"
${SASS} pitagoras/static/sample/css/common.scss:pitagoras/static/sample/css/master.css
