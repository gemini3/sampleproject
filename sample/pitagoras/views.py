# -*- coding: utf-8 -*
# Sample Project - This is a demo project designed to display coding ability.
#                  No permission is given to copy, distribute or reuse the code.
# Copyright (C) 2017  Miha Kitič<miha.kitic@mksp.si>

from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework import permissions

from pitagoras.serializer import TrianglesSerializer

from pitagoras.models import Triangles

import pdb
import math

# This is page entry point
def index(request):
    base_context = {
        'somevar' : "abcd",
    }
    return render(request, 'main.html', base_context)


# This class handles calculations
class Calculations(APIView):
    #authentication_classes = (SessionAuthentication,)
    #permission_classes = (permissions.IsAuthenticated,)

    # Allowed methods
    http_method_names = ['get_list', 'post', 'put', 'delete', ]

    def __recalculate(self, data):

        # Calculate hypothenuse
        ccc = data['aa'] * data['aa'] + data['bb'] * data['bb']
        cc = math.sqrt(ccc)

        return {
            'aa' : data['aa'],
            'bb' : data['bb'],
            'cc' : cc,
        }


    # Extract list of stored values
    def get_list(self, request):
        triangles = Triangles.objects.all()
        tri_ser = TrianglesSerializer(triangles, many=True)
        return Response(tri_ser.data)

    # Handle a PUT request - perform recalculation only
    def put(self, request):
        try:
            # Extract data
            aa = float(request.data['a_len'] or 0)
            bb = float(request.data['b_len'] or 0)

            res = self.__recalculate({
                'aa' : aa,
                'bb' : bb,
            })

            # Response
            return Response({
                'a_len' : res['aa'],
                'b_len' : res['bb'],
                'c_len' : res['cc'],
                'unit'  : request.data['unit'] or 'cm',
            }, status=status.HTTP_200_OK)

        except:
            return Response(status=status.HTTP_404_NOT_FOUND)

    # Handle a POST request - perform recalculation and store data to database...
    # ... then return that same data back to the caller
    def post(self, request):
        try:
            serializer = TrianglesSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()

                # Response
                return Response(serializer.data, status=status.HTTP_200_OK)

            else:
                return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)

        except:
            return Response(status=status.HTTP_404_NOT_FOUND)

    # Request for entry deletion
    def delete(self, request, entry_pk=0):
        try:
            tri = Triangles.objects.get(pk=entry_pk)
            tri.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)

        except:
            return Response(status=status.HTTP_404_NOT_FOUND)
