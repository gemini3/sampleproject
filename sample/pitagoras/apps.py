from django.apps import AppConfig


class PitagorasConfig(AppConfig):
    name = 'pitagoras'
