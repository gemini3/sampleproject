# -*- coding: utf-8 -*
# Sample Project - This is a demo project designed to display coding ability.
#                  No permission is given to copy, distribute or reuse the code.
# Copyright (C) 2017  Miha Kitič<miha.kitic@mksp.si>

from django.conf.urls import url, include
import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^recalculate/$', views.Calculations.as_view()),
    url(r'^table/$', views.Calculations.as_view()),
    url(r'^table/(?P<entry_pk>[0-9]+)/$', views.Calculations.as_view()),
]

