# -*- coding: utf-8 -*
# Sample Project - This is a demo project designed to display coding ability.
#                  No permission is given to copy, distribute or reuse the code.
# Copyright (C) 2017  Miha Kitič<miha.kitic@mksp.si>

from __future__ import unicode_literals
from django.db import models


# This table holds values for lengths and units
class Triangles(models.Model):
    a_len      = models.FloatField(default=0.00)
    b_len      = models.FloatField(default=0.00)
    c_len      = models.FloatField(default=0.00)
    unit       = models.CharField(max_length=10, blank=True, default="cm")

    def __unicode__(self):
        return 'PK:%s UNIT:%s' % (self.pk, self.unit)
