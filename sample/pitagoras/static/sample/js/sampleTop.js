/**
 * Sample Project - This is a demo project designed to display coding ability.
 *                  No permission is given to copy, distribute or reuse the code.
 * Copyright (C) 2017 Miha Kitič <miha.kitic@mksp.si>
**/

/**
  * Ready function called upon document launch. */
function sampleTop() {
    return $$(sampleBase(), {
        model: {
            unit  : "cm",
            a_len : 1.0,
            b_len : 1.0,
            c_len : 1.41,
        },
        view: {
            format: '<div>'

                + '<div class="row" style="height:52px;">'

                    + '<nav class="navbar navbar-default controlbar">'
                        + '<div class="container-fluid">'
                            + '<!-- Brand and toggle get grouped for better mobile display -->'

                            // Mobile menu
                            + '<div class="navbar-header">'
                                + '<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#service-2-detail-view-controlbar" aria-expanded="false">'
                                    + '<span class="sr-only">Toggle navigation</span>'
                                    + '<span class="icon-bar"></span>'
                                    + '<span class="icon-bar"></span>'
                                    + '<span class="icon-bar"></span>'
                                + '</button>'
                            + '</div>'

                            // Big screen content
                            + '<!-- Collect the nav links, forms, and other content for toggling -->'
                            + '<div class="collapse navbar-collapse" id="service-2-detail-view-controlbar">'

                                // Control bar components
                                + '<form class="navbar-form form-inline navbar-left quotation-offer-view">'

                                    + '<div class="form-group">'
                                        + '<div class="input-group" >'
                                            + '<span class="text-size-6 input-group-addon controlbar-components"'
                                            + '      style="width:2em;">'
                                                + 'A:'
                                            + '</span>'
                                            + '<input type="search"'
                                            + '       style="width: 3em; text-align: center;"'
                                            + '       class="form-control controlbar-components"'
                                            + '       data-bind="a_len"'
                                            + '       title="Length of side A"'
                                            + '       placeholder="Real number">'
                                            + '<span class="text-size-6 input-group-addon controlbar-components"'
                                            + '      style="width:2em;"'
                                            + '      data-bind="unit">'
                                            + '</span>'
                                        + '</div>'
                                    + '</div>'

                                    + '<div class="form-group">'
                                        + '<div class="input-group" >'
                                            + '<span class="text-size-6 input-group-addon controlbar-components"'
                                            + '      style="width:2em;">'
                                                + 'B:'
                                            + '</span>'
                                            + '<input type="search"'
                                            + '       style="width: 3em; text-align: center;"'
                                            + '       class="form-control controlbar-components"'
                                            + '       data-bind="b_len"'
                                            + '       title="Length of side B"'
                                            + '       placeholder="Real number">'
                                            + '<span class="text-size-6 input-group-addon controlbar-components"'
                                            + '      style="width:2em;"'
                                            + '      data-bind="unit">'
                                            + '</span>'
                                        + '</div>'
                                    + '</div>'

                                    + '<div class="form-group">'
                                        + '<div class="input-group" >'
                                            + '<span class="text-size-6 input-group-addon controlbar-components"'
                                            + '      style="width:2em;">'
                                                + 'C:'
                                            + '</span>'
                                            + '<input type="search"'
                                            + '       style="width: 3em; text-align: center;"'
                                            + '       class="form-control controlbar-components"'
                                            + '       data-bind="c_len"'
                                            + '       title="Length of side C"'
                                            + '       placeholder="Real number" disabled>'
                                            + '<span class="text-size-6 input-group-addon controlbar-components"'
                                            + '      style="width:2em;"'
                                            + '      data-bind="unit">'
                                            + '</span>'
                                        + '</div>'
                                    + '</div>'

                                    + '<div class="form-group">'
                                        + '<div class="input-group">'
                                            + '<span class="input-group-addon controlbar-components"'
                                            + '      style="width: 3em;">'
                                                + 'UNIT'
                                            + '</span>'
                                            + '<select data-bind="unit"'
                                            + '        class="form-control controlbar-components"'
                                            + '        title="Chose a dashboard type">'
                                                + '<option value="mm">mm</option>'
                                                + '<option value="cm">cm</option>'
                                                + '<option value="dm">dm</option>'
                                                + '<option value="m">m</option>'
                                            + '</select>'
                                        + '</div>'
                                    + '</div>'

                                    + '<div class="form-group">'
                                        + '<div class="btn-group" role="group" aria-label="...">'
                                            + '<button type="button" class="btn btn-default controlbar-components-btn">'
                                                + '<span class="glyphicon glyphicon-save"'
                                                + '      data-toggle="tooltip"'
                                                + '      title="Store values to database"></span>'
                                            + '</button>'
                                        + '</div>'
                                    + '</div>'


                                + '</form>'

                            + '</div><!-- /.navbar-collapse -->'
                        + '</div><!-- /.container-fluid -->'
                    + '</nav>'
                + '</div>'

                + '<div class="row" style="height: calc(100% - 48px);">'

                    + '<div class="col-sm-4">'
                        + '<div class="table-placeholder"></div>'
                    + '</div>'

                    + '<div class="col-sm-8">'
                        + '<div class="graphic-placeholder"></div>'
                    + '</div>'

                + '</div>'

            + '</div>',

            style: '& {'
                 + '    height  : 100%;'
                 + '    padding : 5px;'
                 + '} '

                 + '& .row {'
                 + '    width   : 100%;'
                 + '    margin  : 0px;'
                 + '} '

                 + '& [class*="col-sm-"] {'
                 + '        padding-left  : 0px !important;'
                 + '        padding-right : 0px !important;'
                 + '} '

                 + '& nav, & .row > div, & .table-placeholder, & .graphic-placeholder {'
                 + '    height  : 100%;'
                 + '} '

                //+ '& div { border: 1px solid darkgrey; }'

                 + '& .form-group, & .navbar-form {'
                 + '    margin : 4px 8px 4px 2px;'
                 + '} '
        },
        controller: {

            /**
             * Ran when object instantiated. */
            'create' : function() {
                this.recalculate();

                // Instantiate data-table
                this.table = sampleTable();
                this.append(this.table, '.table-placeholder');
                this.table.createTable();

                // Instantiate visualization object
                this.visual = sampleSVG();
                this.append(this.visual, '.graphic-placeholder');
            },

            /**
             * Capture change of input fields. */
            'change:a_len' : function() {
                if (this.table != null) { this.table.clearSelect(); }

                if (this.visual != null) {
                    var dt = this.model.get();
                    this.visual.recalculate(parseFloat(dt.a_len),
                                            parseFloat(dt.b_len),
                                            parseFloat(dt.c_len),
                                            dt.unit);
                }

                this.recalculate();
            },

            'change:b_len' : function() {
                if (this.table != null) { this.table.clearSelect(); }

                if (this.visual != null) {
                    var dt = this.model.get();
                    this.visual.recalculate(parseFloat(dt.a_len),
                                            parseFloat(dt.b_len),
                                            parseFloat(dt.c_len),
                                            dt.unit);
                }

                this.recalculate();
            },

            'change:unit'  : function() {
                if (this.table != null) { this.table.clearSelect(); }

                if (this.visual != null) {
                    var dt = this.model.get();
                    this.visual.recalculate(parseFloat(dt.a_len),
                                            parseFloat(dt.b_len),
                                            parseFloat(dt.c_len),
                                            dt.unit);
                }

                this.recalculate();
            },

            /**
             * Detect 'click' on control bar elements & select all. */
            'click [data-bind*="_len"]' : function(e) {
                e.target.select();
            },

            /**
             * Store value into database. */
            'click .glyphicon-save' : function(e) {
                this.store();
            },

            /**
             * Catching selection from table. */
            'child:sample-table-row-selected' : function(e, dt) {
                if (this.visual == null) { return; }

                this.visual.recalculate(dt.a_len, dt.b_len, dt.c_len, dt.unit);
            }
        },

        // Variables
        table  : null,
        visual : null,

        /**
         * Function to recalculate values. */
        recalculate: function() {
            var ctx = this;
            var mdl = this.model.get();

            ajaxSubmitDataCall('PUT',
                               '/recalculate/',
                               {},
                               mdl,
                               function(result, response) {

                // On success...
                if (result) {
                    ctx.model.set(response, {silent:true});
                    ctx.view.sync();

                // On failure
                } else {
                    console.log('Failed to calculate results');
                    ctx.model.set({
                        c_len : '??',
                    });
                }
            });
        },

        /**
         * This function will store data to database. */
        store: function() {
            var ctx = this;
            var mdl = this.model.get();

            ajaxSubmitDataCall('POST',
                               '/recalculate/',
                               {},
                               mdl,
                               function(result, response) {

                // On success...
                if (result) {
                    ctx.model.set(response, {silent:true});
                    ctx.view.sync();

                    // Also reload table
                    ctx.table.reload();

                // On failure
                } else {
                    console.log('Failed to calculate results');
                    ctx.model.set({
                        c_len : '??',
                    });
                }
            });
        },

        /**
         * Initi function to be called when top-level object instantiated. */
        init: function() {
            this.visual.recalculate(
                this.model.get('a_len'),
                this.model.get('b_len'),
                this.model.get('c_len'),
                this.model.get('unit'),
            );
        },
    });
}
