/**
 * Sample Project - This is a demo project designed to display coding ability.
 *                  No permission is given to copy, distribute or reuse the code.
 * Copyright (C) 2017 Miha Kitič <miha.kitic@mksp.si>
**/

/**
 * Ready function called upon document launch. */
$(document).ready(function() {

    // AJAX setup - consider whether this should go to TOOLS JS file.
    ajaxCommonSetup();

    // Initialize JS object
    var top_obj = sampleTop();
    $$.document.append(top_obj, "#sample-application-main-placeholder");
    top_obj.init();
});

/**
  * The function bellow can eb used to extract CSRF token from the
  * page. Presumably this is needed for Ajax requests.
  * \param name Name of the cookie where CSRF can be found. */
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

/**
  * This function returns methods, that do not require CSRF.
  * Since CSRF token should be kept secret as much as possible, it
  * makes sense to only use it, when it is required.
  * \param method Name of the method that we will test. */
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}


/**
  * This is common AJAX setup for all pages. */
function ajaxCommonSetup() {
    $.ajaxSetup({

        // Called before data is sent
        beforeSend: function(xhr, settings) {

            // Modify request header - add CSRF token.
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
}

/**
 * This function will process parameters before constructing AJAX collect
 * line. At the moment this function does the following:
 *  # changes application date format to server date format
 *  # Replaces parameters characters (&, %,?) to URI notation
 *
 * @param address_suffix Suffix to the web page address that determines
 *                       action on server side.
 * @param params JS object containing parameters as key:value pairs. */
function _ajaxProcessParams(address_suffix, params) {

    // Create address string
    var addr_string = address_suffix;
    var param_num = 0;

    // Loop through properties and construct address string
    for (var key in params) {
        if (params.hasOwnProperty(key)) {

            // Insert a param separator
            addr_string += (param_num++ <= 0) ? '?' : '&';
            addr_string += key + "=" + encodeURIComponent(params[key]);
        }
    }

    return addr_string;
}

/**
 * Function to contact server via AJAX call. */
function ajaxSubmitDataCall(method, address_suffix, params, model, callback) {
    var url = _ajaxProcessParams(address_suffix, params);

    // Perform AJAX call to DJANGO db
    var request = $.ajax({
        type:     method,
        url:      url,
        cache:    true,
        async:    true,
        data:     JSON.stringify(model),
        dataType: "json",
        contentType: "application/json"
    });

    // Callback that is called when request succeeds
    request.done(function(response) {
        callback(true, response);
    });

    // Callback that is called when request succeeds
    request.fail(function(response) {
        callback(false, response);
    });
}
