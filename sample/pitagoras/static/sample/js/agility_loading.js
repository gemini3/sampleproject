/**
 * Sample Project - This is a demo project designed to display coding ability.
 *                  No permission is given to copy, distribute or reuse the code.
 * Copyright (C) 2017 Miha Kitič <miha.kitic@mksp.si>
**/


/**
 * This is an Agility.js component to display 'LOADING' sign when certain
 * part of application is being loaded. */
function agilityLoading(params) {
    return $$(sampleBase(), {
        model: {
            loading_message : "Loading...",
            error_message   : "Error!",

            __message       : ""
        },
        view: {
            format: '<div>'
                + '<span data-bind="__message">Loading...</span>'
            + '</div>',
            style: '& {'
                 + '        height       : 100%;'
                 + '        width        : 100%;'
                 + '        display      : table;'
                 + '        text-align   : center;'
                 + '        font-size    : 750%;'
                 + '        border       : 5px dashed #a0b2bd;'
                 + '        border-radius: 30px;'
                 + '} '

                 + '& span {'
                 + '        display: table-cell;'
                 + '        vertical-align: middle;'
                 + '} '
        },
        controller: {

            /**
             * Initial variables are set here. */
            'create': function() {
                if (typeof params !== 'undefined') { this.model.set(params) };
            },
        },


        ///
        /// =============  Agility variables  ==============
        ///


        ///
        /// =============  Getters / Setters  ==============
        ///
        setLoading: function() {
            this.model.set({
                __message : this.model.get('loading_message'),
            });
        },

        setError: function() {
            this.model.set({
                __message : this.model.get('error_message'),
            });
        },

    });
}
