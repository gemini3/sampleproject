/**
 * Sample Project - This is a demo project designed to display coding ability.
 *                  No permission is given to copy, distribute or reuse the code.
 * Copyright (C) 2017 Miha Kitič <miha.kitic@mksp.si>
**/

/**
  * This object is used to visualize triangle. */
function sampleSVG() {
    return $$(sampleBase(), {
        model: {
            unit  : "cm",
            a_len : 1.0,
            b_len : 1.0,
            c_len : 1.41,

            // Triangle coordinates
            triangle_ABC : "30,60 70,60 70,20",
            square_a     : "100,60 97,61 97,59",
            square_b     : "100,60 97,61 97,59",
            square_c     : "100,60 97,61 97,59",
            a_txt        : "a",
            a_txt_x      : "10",
            a_txt_y      : "10",

            b_txt        : "b",
            b_txt_x      : "10",
            b_txt_y      : "10",

            c_txt        : "c",
            c_txt_x      : "10",
            c_txt_y      : "10",

            // CS
            x_x1         : "0",
            x_x2         : "0",
            x_y1         : "0",
            x_y2         : "0",

            y_x1         : "0",
            y_x2         : "0",
            y_y1         : "0",
            y_y2         : "0",

            x_tip        : "100,60 97,61 97,59",
            x_tip_txt_x  : "10",
            x_tip_txt_y  : "10",

            y_tip        : "100,60 97,61 97,59",
            y_tip_txt_x  : "10",
            y_tip_txt_y  : "10",

            // Styles
            sfc_triangle : "fill:lime;stroke:purple;stroke-width:2",
            sfc_square   : "fill:red;stroke:pink;stroke-width:2;fill-opacity:0.1;stroke-opacity:0.9",
            sfc_cs_lines : "stroke:rgb(120,20,20);stroke-width:2",
            sfc_cs_tips  : "fill:rgb(120,20,20);stroke:rgb(120,20,20);stroke-width:1",
            sfc_cs_text  : "fill:rgb(120,20,20);stroke:rgb(120,20,20);stroke-width:1;",
        },
        view: {
            format: '<div>'
                + '<div id="svg-container" style="width:100%;height:100%;">'

                    + '<svg width="100%"'
                    + '     height="100%"'
                    //+ '     viewBox="0 0 100 100"'
                    + '     preserveAspectRatio="xMidYMid" >'

                        // Squares
                        + '<polygon class="sfc-square" data-bind=",points=square_a, style=sfc_square" />'
                        + '<polygon class="sfc-square" data-bind=",points=square_b, style=sfc_square" />'
                        + '<polygon class="sfc-square" data-bind=",points=square_c, style=sfc_square" />'

                        // Coordinate X
                        + '<line data-bind=",style=sfc_cs_lines, x1=x_x1, y1=x_y1, x2=x_x2, y2=x_y2" />'
                        + '<polygon data-bind=",style=sfc_cs_tips, points=x_tip" />'
                        + '<text data-bind=",style=sfc_cs_text, x=x_tip_txt_x, y=x_tip_txt_y">x</text>'

                        // Coordinate Y
                        + '<line data-bind=",style=sfc_cs_lines, x1=y_x1, y1=y_y1, x2=y_x2, y2=y_y2" />'
                        + '<polygon data-bind=",style=sfc_cs_tips, points=y_tip"  />'
                        + '<text data-bind=",style=sfc_cs_text, x=y_tip_txt_x, y=y_tip_txt_y">y</text>'

                        // Triangle
                        + '<polygon data-bind=",points=triangle_ABC, style=sfc_triangle" />'
                        + '<text data-bind="a_txt,style=sfc_cs_text, x=a_txt_x, y=a_txt_y"></text>'
                        + '<text data-bind="b_txt,style=sfc_cs_text, x=b_txt_x, y=b_txt_y"></text>'
                        + '<text data-bind="c_txt,style=sfc_cs_text, x=c_txt_x, y=c_txt_y"></text>'
                    + '</svg>'
                + '</div>'
            + '</div>',

            style: '& {'
                 + '    height  : 100%;'
                 + '    padding : 6px 0px 6px 6px;'
                 + '} '

                 + '& div#svg-container {'
                 + '    background-color : #f9f9f8;'
                 + '    padding          : 2px 7px;'
                 + '    border-radius    : 4px;'
                 + '    border           : 1px solid #e7e7e7;'
                 + '    height           : 100%;'
                 + '    width            : 100%;'
                 + '}'

                // SVG related
                 + '& svg {'
                 //+ '    margin           : 7% auto;'
                 + '    display          : block;'
                 + '} '
        },
        controller: {

            /**
             * Ran when object instantiated. */
            'create' : function() {

            },
        },

        // Variables
        w_cnt      : 100.0,
        h_cnt      : 100.0,
        aspect_cnt : 1.00,

        aspect_tri : 1.00,
        TNR        : 0.30,  // Triangle Normalized Rating - against normalized side

        recalculate: function(a, b, c, unit) {

            // Get size of SVG container & determine aspect
            this.w_cnt = $('#svg-container').width();
            this.h_cnt = $('#svg-container').height();
            this.aspect_cnt = this.h_cnt / this.w_cnt;

            // calculate aspect of triangle
            this.aspect_tri = b / a;

            // If aspect of triangle is more than the aspect of display,
            // then side 'b' will be the base for triangle normalization...
            // else we use side 'a' as a base.
            if (this.aspect_tri < this.aspect_cnt) {
                var a_mod = this.TNR * this.w_cnt;
                var norm  = a_mod / a;

                var b_mod = norm * b;
                var c_mod = norm * c;

            } else {
                var b_mod = this.TNR * this.h_cnt;
                var norm  = b_mod / b;

                var a_mod = norm * a;
                var c_mod = norm * c;
            }

            // Determine triangle points
            var A_x = (this.w_cnt - a_mod) / 2;
            var A_y = A_x * this.aspect_cnt;

            var B_x = A_x + a_mod;
            var B_y = A_y;

            var C_x = A_x + a_mod;
            var C_y = A_y + b_mod;

            // A_suqare - extra points
            var Ba_x = B_x;
            var Ba_y = B_y - a_mod;

            var Aa_x = A_x;
            var Aa_y = A_y - a_mod;

            // B_suqare - extra points
            var Bb_x = B_x + b_mod;
            var Bb_y = B_y;

            var Cb_x = C_x + b_mod;
            var Cb_y = C_y;

            // C_suqare - extra points
            var Ac_x = A_x - b_mod;
            var Ac_y = A_y + a_mod;

            var Cc_x = Ac_x + a_mod;
            var Cc_y = Ac_y + b_mod;



            // Finally update the model
            this.model.set({
                triangle_ABC : A_x + "," + this.__correct_y(A_y) + " "
                             + B_x + "," + this.__correct_y(B_y) + " "
                             + C_x + "," + this.__correct_y(C_y),

                square_a     : A_x  + "," + this.__correct_y(A_y)  + " "
                             + B_x  + "," + this.__correct_y(B_y)  + " "
                             + Ba_x + "," + this.__correct_y(Ba_y) + " "
                             + Aa_x + "," + this.__correct_y(Aa_y) + " ",

                square_b     : C_x  + "," + this.__correct_y(C_y)  + " "
                             + B_x  + "," + this.__correct_y(B_y)  + " "
                             + Bb_x + "," + this.__correct_y(Bb_y) + " "
                             + Cb_x + "," + this.__correct_y(Cb_y) + " ",

                square_c     : A_x  + "," + this.__correct_y(A_y)  + " "
                             + C_x  + "," + this.__correct_y(C_y)  + " "
                             + Cc_x + "," + this.__correct_y(Cc_y) + " "
                             + Ac_x + "," + this.__correct_y(Ac_y) + " ",

                // CS
                x_x1         : "0.0",
                x_x2         : this.w_cnt,
                x_y1         : this.__correct_y(A_y),
                x_y2         : this.__correct_y(A_y),

                y_x1         : A_x,
                y_x2         : A_x,
                y_y1         : this.__correct_y(0),
                y_y2         : this.__correct_y(this.h_cnt),

                // Tips
                x_tip        : this.w_cnt      + "," + this.__correct_y(A_y)   + " "
                             + (this.w_cnt-10) + "," + this.__correct_y(A_y+4) + " "
                             + (this.w_cnt-10) + "," + this.__correct_y(A_y-4) + " ",
                x_tip_txt_x  : (this.w_cnt-10),
                x_tip_txt_y  : this.__correct_y(A_y-14),


                y_tip        : A_x             + "," + this.__correct_y(this.h_cnt)   + " "
                             + (A_x-4)        + "," + this.__correct_y(this.h_cnt-10) + " "
                             + (A_x+4)        + "," + this.__correct_y(this.h_cnt-10) + " ",
                y_tip_txt_x  : (A_x-14),
                y_tip_txt_y  : this.__correct_y(this.h_cnt-6),

                // Triangle text
                a_txt        : "a = " + a.toFixed(2) + " " + unit,
                a_txt_x      : A_x + a_mod/3,
                a_txt_y      : this.__correct_y(A_y - 15),

                b_txt        : "b = " + b.toFixed(2) + " " + unit,
                b_txt_x      : B_x + 15,
                b_txt_y      : this.__correct_y(B_y + b_mod/2 - 5),

                c_txt        : "c = " + c.toFixed(2) + " " + unit,
                c_txt_x      : A_x + a_mod/6,
                c_txt_y      : this.__correct_y(A_y + b_mod/2 + 20),


            });
        },

        /**
         * Function to reverse Y coordinate. */
        __correct_y: function(y) {
            return this.h_cnt - y;
        },
    });
}
