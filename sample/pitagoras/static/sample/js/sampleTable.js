/**
 * Sample Project - This is a demo project designed to display coding ability.
 *                  No permission is given to copy, distribute or reuse the code.
 * Copyright (C) 2017 Miha Kitič <miha.kitic@mksp.si>
**/

/**
  * Ready function called upon document launch. */
function sampleTable() {

    var col_config_list = [];
    var col_ordering    = [[ 3, "asc" ], ];

    // Len A column
    var col_a = {
        "data"      : "a_len",
        "className" : "text-center",
        "orderable" : true,
        "render"    : function(data, type, row) {
            return parseFloat(data).toFixed(2);
        }
    };

    // Len B column
    var col_b = {
        "data"      : "b_len",
        "className" : "text-center",
        "orderable" : true,
        "render"    : function(data, type, row) {
            return parseFloat(data).toFixed(2);
        }
    };

    // Len C column
    var col_c = {
        "data"      : "c_len",
        "className" : "text-center",
        "orderable" : true,
        "render"    : function(data, type, row) {
            return parseFloat(data).toFixed(4);
        }
    };

    // Unit column
    var col_unit = {
        "data"      : "unit",
        "className" : "text-center",
        "orderable" : true,
    };

    // Unit column
    var col_control = {
        "data"      : "unit",
        "className" : "text-center",
        "orderable" : false,
        "render"    : function(data, type, row) {
            return '<div class="btn-group" style="">'

                + '<button class="btn btn-default" type="button" aria-controls="delete"'
                + '        title="Delete this entry" data-toggle="tooltip">'
                    + '<span class="glyphicon glyphicon-trash" style="margin-top:1px; margin-right:1px;" />'
                + '</button>'

            + '</div>';
        }
    };

    col_config_list.push(col_a);
    col_config_list.push(col_b);
    col_config_list.push(col_c);
    col_config_list.push(col_unit);
    col_config_list.push(col_control);

    // Format string
    format_string = '<div>'
        + '<div class="el_loading"></div>'
        + '<div class="el_content">'
            + '<table id="sampleTable" class="display nowrap order-column" width="100%" cellspacing="0">'
                + '<thead>'
                    + '<tr>'
                        + '<th>A</th>'              // A - side length
                        + '<th>B</th>'              // B - side length
                        + '<th>C</th>'              // C - hypothenuse length
                        + '<th>Unit</th>'           // Unit
                        + '<th class="links"></th>' // Control field
                    + '</tr>'
                + '</thead>'
            + '</table>'
        + '</div>'
    + '</div>';

    return $$(sampleBase(), {
        model: {
        },
        view: {
            format: format_string,

            style: // Flexbox into DataTables
                   '& , & .el_loading, & .el_content { height:100%; }'
                 + '& .el_content {'
                 + '        padding: 6px 0px;'
                 + '} '

                 + '& #sampleTable_wrapper {'
                 + '        background-color : #f9f9f8;'
                 + '        padding          : 2px 7px;'
                 + '        border-radius    : 4px;'
                 + '        border           : 1px solid #e7e7e7;'
                 + '        height           : 100%;'
                 + '} '

                 + '& .dataTables_scroll {'
                 + '        height: 100%;'
                 + '        display: flex;'
                 + '        flex-direction: column;'
                 + '} '

                 + '& .dataTables_scrollHead {'
                 + '        flex: 0;'
                 + '        min-height: 38px;'
                 + '} '

                 + '& .dataTables_scrollBody {'
                 + '        flex-basis: 0;'
                 + '        flex-grow: 1;'
                 + '        flex-shrink: 1;'
                 + '} '

                 + '& .dataTables_empty { display : none; } '

                 + '& .btn-group {'
                 + '        margin: 2px;'
                 + '} '

                 // Font & table design
                 + '& table {'
                 + '        font-size:150%;'
                 + '        color: #484747;'
                 + '} '

                 + '& thead {'
                 + '        background-color: #efefef;'
                 + '} '

                 + '& th, td { padding:6px; }'

                 + '& th[class*="sorting"]:after {'
                 + '        bottom : 5px !important;'
                 + '        right  : 5px !important;'
                 + '} '

                 + '& tr.odd {'
                 + '        background-color:#f9f9f8;'
                 + '} '

                 + '& tr.even {'
                 + '        background-color:#efefef;'
                 + '} '
        },
        controller: {

            /**
             * Ran when object instantiated. */
            'create' : function() {

                // Initialize stuff here...
                this.loading_screen = agilityLoading();
                this.append(this.loading_screen, '.el_loading');
                this.__displayLoading();
            },
        },

        // Variables
        table          : null,
        loading_screen : null,

        /**
         * Function will cause datatable to reload. */
        reload: function() {
            if (this.table == null) { return; }
            this.table.ajax.reload();
        },

        /**
         * Function to remove select if new value is entered into control bar. */
        clearSelect: function() {
            this.view.$('#sampleTable tr').removeClass('selected');
        },

        /**
         * Function creates data table. */
        createTable: function() {
            var context = this;

            // Initialize dataTable
            this.table = this.view.$('#sampleTable').DataTable({

                // Loading table data
                "ajax": function(data, callback, settings) {
                    var address = '/table/';
                    context.__displayLoading();

                    // Trigger AJAX call to get services
                    ajaxSubmitDataCall(
                             'GET_LIST',
                              address,
                              {},
                              {},
                              function(success, response) {

                        // In case of success (success == true)
                        if (success) {

                            // Re-Enable event list here!
                            context.__displayContent();

                            // Callback to submit received data to DataTables
                            callback({ data:response });

                        // In case of failure, user should be able to correct errors
                        // Do not remove console log here!
                        } else {
                            context.__displayError();
                        }
                    });
                },

                // Columns that will be shown in the table
                "columns": col_config_list,

                dom: 'T<"clear">rt',
                "order": col_ordering,
                'select' : true,

                // Table is scrollable vertically - 300px max
                "scrollY":        "100%",

                // Table is NOT scrollable horizontally
                "scrollX":        false,

                // ??
                "scrollCollapse": false,

                // ??
                paging:false,
                lengthChange:false,

                // Display visible data before everything is loaded
                "deferRender":    true,

                // Destroy old table when re-initialized (Do not reinit if possible)
                "destroy":        true,     // With this, we can reinitialize already
                                            // initialized table

                // Do not display search window
                "searching":      false,

            });

            /**
             * Datatable line on-click handler to handle element deletion. */
            this.view.$('#sampleTable').on('click', 'button[aria-controls="delete"]', function (event) {
                event.preventDefault();

                // Extract some data from the row
                var tr = $(this).closest('tr');
                var row = context.table.row( tr );
                if (! row.data()) { return; }
                var dt = row.data();

                // Trigger AJAX call to delete given quote
                ajaxSubmitDataCall('DELETE',
                                   '/table/' + dt.pk + '/',
                                   {},              // No parameters to send over GET
                                   {},
                                   function(success, response) {
                    if (!success) { alert('Failed to delete entry!'); }
                    context.reload();
                });
            });

            /**
             * Datatable line on-click handler to select element. */
            this.view.$('#sampleTable').on('click', 'td', function (event) {
                event.preventDefault();

                // Extract some data from the row
                var tr = $(this).closest('tr');
                var row = context.table.row( tr );
                if (! row.data()) { return; }
                var dt = row.data();

                context.view.$('#sampleTable tr').removeClass('selected');
                context.view.$(tr).addClass('selected');

                context.trigger('sample-table-row-selected', dt);
            });
        },
    });
}
