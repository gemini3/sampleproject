/**
 * Sample Project - This is a demo project designed to display coding ability.
 *                  No permission is given to copy, distribute or reuse the code.
 * Copyright (C) 2017 Miha Kitič <miha.kitic@mksp.si>
**/

/**
 * This is a prototype agility object each object should extend.
 *
 * It holds a set of functions that every object needs:
 *   -> close()            ::  Will close this and all appended objects
 *   -> closeAppended(obj) ::  Will cause appended object to close, but only
 *                             if it exists. */
function sampleBase() {
    return $$({
        model: {},
        view: {},
        controller: {},

        /**
         * Hide event-list & display 'Loading' */
        __displayLoading: function() {
            this.loading_screen.setLoading();
            this.view.$('.el_loading').show();
            this.view.$('.el_content').hide();
        },

        /**
         * Display error screen in case of bad data loaded. */
        __displayError: function() {
            this.loading_screen.setError();
            this.view.$('.el_loading').show();
            this.view.$('.el_content').hide();
        },

        /**
         * Hide 'Loading' & display event-list */
        __displayContent: function() {
            this.view.$('.el_loading').hide();
            this.view.$('.el_content').show();
        },

        /**
         * This function will remove particular object and make it null,
         * but only if it exists. */
        closeAppended: function(obj) {
            if (obj === undefined || obj == null) { return obj; }
            obj.close();
            return null;
        },

        /**
         * This function is used to reduce the load of multiple Agility
         * objects once they are not needed any more. */
        close: function() {
            this.each(function() { this.close(); });
            this.destroy();
        },
    });
}
