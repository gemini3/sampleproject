# -*- coding: utf-8 -*
# Sample Project - This is a demo project designed to display coding ability.
#                  No permission is given to copy, distribute or reuse the code.
# Copyright (C) 2017  Miha Kitič<miha.kitic@mksp.si>

from rest_framework import serializers

from django.contrib.auth.models import User
from pitagoras.models import Triangles

import pdb


##
# Serializer for Triangles model
class TrianglesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Triangles
        fields = ('a_len', 'b_len', 'c_len', 'unit', 'pk', )

    # Make sure that values for sides are float
    def validate_a_len(self, value):
        try:
            return float(value)
        except:
            raise serializers.ValidationError("Value for side 'a' must be float!")

    # Make sure that values for sides are float
    def validate_b_len(self, value):
        try:
            return float(value)
        except:
            raise serializers.ValidationError("Value for side 'b' must be float!")

    # Make sure that agent_II_PK is not null
    def validate_unit(self, value):
        if value == 'mm' or \
           value == 'cm' or \
           value == 'dm' or \
           value == 'm'  :
           return value
        else:
            raise  serializers.ValidationError(value + " is not allowed value. Allowed values are [mm], [cm], [dm], or [m].")


