# -*- coding: utf-8 -*
# Sample Project - This is a demo project designed to display coding ability.
#                  No permission is given to copy, distribute or reuse the code.
# Copyright (C) 2017  Miha Kitič<miha.kitic@mksp.si>

from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('pitagoras.urls')),
]
